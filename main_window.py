


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton

# class initial_window(object):
#     def window(self, MainWindow):
#         MainWindow.setObjectName("MainWindow")
#         MainWindow.resize(800, 600)
#         # sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
#         # sizePolicy.setHorizontalStretch(0)
#         # sizePolicy.setVerticalStretch(0)
#         # sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
#         # MainWindow.setSizePolicy(sizePolicy)
#         # self.centralwidget = QWidget(MainWindow)
#         # self.centralwidget.setObjectName("centralwidget")
#         self.button1 = QPushButton(self.centralwidget)
#         self.button1.setText("Take Screenshot")
#         self.button1.move(64,32)
#         self.button1.clicked.connect(self.button1_clicked)
#         MainWindow.setCentralWidget(self.centralwidget)
#         self.retranslateUi(MainWindow)
#         QtCore.QMetaObject.connectSlotsByName(MainWindow)

#     def retranslateUi(self, MainWindow):
#         _translate = QtCore.QCoreApplication.translate
#         MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))

#     def button1_clicked():
#         print("button1")


# class Ui_MainWindow(object):
#     def setupUi(self, MainWindow):
#         MainWindow.setObjectName("MainWindow")
#         MainWindow.resize(800, 600)
#         sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
#         sizePolicy.setHorizontalStretch(0)
#         sizePolicy.setVerticalStretch(0)
#         sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
#         MainWindow.setSizePolicy(sizePolicy)
#         self.centralwidget = QtWidgets.QWidget(MainWindow)
#         self.centralwidget.setObjectName("centralwidget")
#         self.start_button = QtWidgets.QPushButton(self.centralwidget)
#         self.start_button.setGeometry(QtCore.QRect(210, 170, 121, 25))
#         sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
#         sizePolicy.setHorizontalStretch(0)
#         sizePolicy.setVerticalStretch(0)
#         sizePolicy.setHeightForWidth(self.start_button.sizePolicy().hasHeightForWidth())
#         self.start_button.setSizePolicy(sizePolicy)
#         self.start_button.setObjectName("start_button")
#         self.stop_button = QtWidgets.QPushButton(self.centralwidget)
#         self.stop_button.setGeometry(QtCore.QRect(360, 170, 121, 25))
#         self.stop_button.setObjectName("stop_button")
#         MainWindow.setCentralWidget(self.centralwidget)
#         self.menubar = QtWidgets.QMenuBar(MainWindow)
#         self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
#         self.menubar.setObjectName("menubar")
#         MainWindow.setMenuBar(self.menubar)
#         self.statusbar = QtWidgets.QStatusBar(MainWindow)
#         self.statusbar.setObjectName("statusbar")
#         MainWindow.setStatusBar(self.statusbar)

#         self.retranslateUi(MainWindow)
#         QtCore.QMetaObject.connectSlotsByName(MainWindow)

#     def retranslateUi(self, MainWindow):
#         _translate = QtCore.QCoreApplication.translate
#         MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
#         self.start_button.setText(_translate("MainWindow", "Start"))
#         self.stop_button.setText(_translate("MainWindow", "Stop"))


# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     MainWindow = QtWidgets.QMainWindow()
#     ui = Ui_MainWindow()
#     ui.setupUi(MainWindow)
#     MainWindow.show()
#     # main_ui = initial_window()
#     # main_ui.window(MainWindow)
#     sys.exit(app.exec_())


import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QToolTip, QMessageBox, QLabel

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.start_button = QtWidgets.QPushButton(self.centralwidget)
        self.start_button.setGeometry(QtCore.QRect(210, 170, 121, 25))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.start_button.sizePolicy().hasHeightForWidth())
        self.start_button.setSizePolicy(sizePolicy)
        self.start_button.setObjectName("start_button")
        self.stop_button = QtWidgets.QPushButton(self.centralwidget)
        self.stop_button.setGeometry(QtCore.QRect(360, 170, 121, 25))
        self.stop_button.setObjectName("stop_button")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.start_button.setText(_translate("MainWindow", "Start"))
        self.stop_button.setText(_translate("MainWindow", "Stop"))


# class window3():
#     print("3")

# class window4():
#     print("4")

# class window5():
#     print("5")

class Window(QMainWindow):
    def __init__(self):
        super().__init__()

        self.title = "First Window"
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500

        self.pushButton = QPushButton("Select Screen", self)
        self.pushButton.move(100, 200)
        self.pushButton.clicked.connect(self.window2)              # <===

        self.media_player = QPushButton("Open MediaPlayer", self)
        self.media_player.move(220, 200)
        self.media_player.clicked.connect(self.window3)

        self.settings = QPushButton("Settings", self)
        self.settings.move(340, 200)
        self.settings.clicked.connect(self.window4)

        self.open_images = QPushButton("Open Images", self)
        self.open_images.move(460, 200)
        self.open_images.clicked.connect(self.window5)

        self.main_window()

    def main_window(self):
        self.label = QLabel("Select Your Option", self)
        self.label.move(285, 120)
        self.setWindowTitle(self.title)
        self.setGeometry(self.top, self.left, self.width, self.height)
        self.show()

    def window2(self):   
        import sys
        app = QtWidgets.QApplication(sys.argv)
        MainWindow = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(MainWindow)
        MainWindow.show()                                         # <===
        # self.w = Window2()
        # self.w.show()
        self.hide()

    def window3():
        pass

    def window4():
        pass

    def window5():
        pass

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Ui_MainWindow()
    sys.exit(app.exec())