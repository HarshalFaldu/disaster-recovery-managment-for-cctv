from glob import glob
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5 import QtGui
from PyQt5.QtWidgets import (QWidget, QPushButton,
                             QHBoxLayout, QVBoxLayout, QApplication, QGraphicsScene, QGraphicsView, QFileDialog,
                             QGraphicsPixmapItem)
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QApplication, QSlider

class Slides(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.layout = QVBoxLayout(self)
        self.image_files = glob(QFileDialog.getExistingDirectory(self,'Select Folder')+'/*')
        self.timer = QBasicTimer()
        self.step = 0
        self.delay = 500  # milliseconds
        s = '<>'*300
        # self.label = QLabel(s, self)
        # self.label.setGeometry(10, 30, 640, 480)
        self.step1 = len(self.image_files) - 1
        print(self.step1)

        self.flag_pause = False

        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.scene = QGraphicsScene(self)
        self.view = QGraphicsView(self.scene)
        self.layout.addWidget(self.view)

        self.image = QGraphicsPixmapItem()
        self.scene.addItem(self.image)
        self.view.centerOn(self.image)

        self.playBtn = QPushButton("Play",self)
        self.playBtn.setGeometry(600, 600, 140, 30)
        self.playBtn.clicked.connect(self.timerEvent)
        self.playBtn.setIcon(QtGui.QIcon('play.png'))
        sf = "Slides are shown {} seconds apart"
        self.setWindowTitle(sf.format(self.delay/1000.0))
        self.layout.addWidget(self.playBtn)

        self.pauseBtn = QPushButton('Pause', self)
        self.pauseBtn.setGeometry(500,500,140,30)
        self.pauseBtn.clicked.connect(self.pause) #backword function
        sf = "Slides are shown {} seconds apart"
        self.setWindowTitle(sf.format(self.delay/1000.0))
        self.layout.addWidget(self.pauseBtn)

        self.backwordBtn = QPushButton("Backword", self)
        self.backwordBtn.setGeometry(600, 600, 140, 30)
        self.backwordBtn.clicked.connect(self.backword)
        self.backwordBtn.setIcon(QtGui.QIcon('play.png'))
        sf = "Slides are shown {} seconds apart"
        self.setWindowTitle(sf.format(self.delay / 1000.0))
        self.layout.addWidget(self.backwordBtn)

        self.slider = QSlider(self)
        self.slider.setOrientation(Qt.Horizontal)
        self.slider.setMinimum(0)
        # max is the last index of the image list
        self.slider.setMaximum(len(self.image_files) - 1)
        self.sliderMoved(0)
        self.slider.sliderMoved.connect(self.sliderMoved)
        self.slider.setGeometry(400,400, 550 ,50)
        self.layout.addWidget(self.slider)

        self.slider_speed = QSlider(self)
        self.slider_speed.setOrientation(Qt.Horizontal)
        self.slider_speed.setMinimum(0)
        self.slider_speed.setMaximum(3)
        self.slider_speed.sliderMoved.connect(self.sliderSpeedMoved)
        self.slider_speed.setGeometry(400,400,50,50)
        self.slider_speed.setWindowTitle('Speed')
        self.layout.addWidget(self.slider_speed)


    def sliderSpeedMoved(self,speed):
        print("Speed: ", speed)
        if speed == 1:
            self.delay = 500  # milliseconds
        elif speed == 2:
            self.delay = 1000
        elif speed == 3:
            self.delay = 2000

    def sliderMoved(self, val):
        print("Slider moved to:", val)
        try:
            image = QPixmap(self.image_files[val])
            self.image.setPixmap(image)
        except IndexError:
            print("Error: No image at index", val)

    def timerEvent(self, e=None):
        if self.step >= len(self.image_files):
            self.timer.stop()
            self.playBtn.setText('Again')
            return
        self.timer.start(self.delay, self)
        file = self.image_files[self.step]
        image_name = QPixmap(file)
        print(file)
        self.image.setPixmap(image_name)
        self.setWindowTitle("{} --> {}".format(str(self.step), file))
        self.step += 1

    def pause(self,flag_pause):
        if not self.flag_pause:
            self.flag_pause = True
            self.timer.stop()
        else:
            self.flag_pause = False
            self.timer.start(self.delay, self)
            self.pauseBtn.setText('Resume')

    def backword(self):
        self.step1 = self.step1
        if not self.step1:
            self.timer.stop()
            self.backwordBtn.setText('Completed')
            return
        self.timer.start(self.delay, self)
        file = self.image_files[self.step]
        image_name = QPixmap(file)
        self.image.setPixmap(image_name)
        self.setWindowTitle("{} --> {}".format(str(self.step1), file))
        self.step1 -= 1


app = QApplication([])
w = Slides()
# w.setGeometry(100, 100, 700, 500)
w.showMaximized()
app.exec_()

